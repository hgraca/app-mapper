<?php declare(strict_types=1);

$rootDir = dirname(__FILE__, 3);

$finder = PhpCsFixer\Finder::create()
    ->in([
        $rootDir . '/src',
        $rootDir . '/tests',
    ])
    ->exclude([
        $rootDir . '/tests/StubProjectSrc',
    ])
    ->name('*.php');

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules(require ".php-cs-fixer.rules.php")
    ->setFinder($finder)
    ->setCacheFile($rootDir . '/var/cache/.php_cs.cache');
