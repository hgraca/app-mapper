<?php

declare(strict_types=1);

/*
 * This file is part of the Application mapper application,
 * following the Explicit Architecture principles.
 *
 * @link https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\AppMapper\Core\Port\Configuration;

use Hgraca\PhpExtension\String\StringHelper;

use function is_dir;

final readonly class ComponentDto
{
    public function __construct(private string $name, private string $path, private int $locationX, private int $locationY)
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPath(): string
    {
        return StringHelper::hasBeginning('./', $this->path)
            ? getcwd() . '/' . StringHelper::removeFromBeginning('./', $this->path)
            : $this->path;
    }

    public function getLocationX(): int
    {
        return $this->locationX;
    }

    public function getLocationY(): int
    {
        return $this->locationY;
    }

    public function isDir(): bool
    {
        return is_dir($this->getPath());
    }
}
